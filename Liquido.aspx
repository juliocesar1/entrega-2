﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Liquido.aspx.cs" Inherits="Liquido" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
         <div class="main">
   	 <div class="wrap">
   	 	<div class="preview-page">
   	 	       <div class="section group">
				<div class="cont-desc span_1_of_2">
					<ul class="back-links">
						<li><a href="#">Inicio</a> ::</li>
						<li><a href="#">Paginas de productos</a> ::</li>
						<li>Nombre del producto</li>
						<div class="clear"> </div>
					</ul>
				  <div class="product-details">	
					<div class="grid images_3_of_2">
							<ul id="etalage">
							<li>
								<a href="optionallink.html">
									<img class="etalage_thumb_image" src="images/32.jpg" />

                                    
									
								</a>
							</li>
                                </div>
                      <div class="desc span_3_of_2">
					<h2>LÍQUIDO PARA FRENOS DOT-4</h2>
										
					<div class="price">
						<p>Precio: <span>$8.000</span></p>
					</div>
					<div class="available">
						<ul>
						 
						  <li><span>Presentación:</span>&nbsp;pinta</li>
					    </ul>
					</div>
                      </div>
                    </div>
                       </div>
                </div>
            </div>
         </div>
</asp:Content>

