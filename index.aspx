﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <div class="wrap">
  		     <div class="header_bottom">
			   <div class="slider-text">
			   	<h2>Estación de servicio el chorro </h2>
			   	<p>Siempre sirviendo a la comunidad caqueteña.</p>
			   	</div>
	  	      </div>
	  	      <div class="slider-img">
	  	      	<img src="images/1.png" alt="" />
	  	      </div>
	  	     <div class="clear"></div>
	      </div>
   		</div>
   </div>
   
  <div class="main">

      <div class="content">
    	        <div class="content_top">
    	        	<div class="wrap">
		          	   <h3>Nuestras instalaciones</h3>
		          	</div>
		          	<div class="line"> </div>
		          	<div class="wrap">
		          	 <div class="ocarousel_slider">  
	      				<div class="ocarousel example_photos" data-ocarousel-perscroll="3">
			                <div class="ocarousel_window">
			                   <a href="#" title="img1"> <img src="images/20.png" alt="" /></a>
			                   <a href="#" title="img2"> <img src="images/21.jpg" alt="" /></a>
			                   <a href="#" title="img3"> <img src="images/23.jpg" alt="" /></a>
			                   <a href="#" title="img4"> <img src="images/24.jpg" alt="" /></a>
			                   <a href="#" title="img5"> <img src="images/25.jpg" alt="" /></a>
			                   
			                </div>
			               <span>           
			                <a href="#" data-ocarousel-link="left" style="float: left;" class="prev"> </a>
			                <a href="#" data-ocarousel-link="right" style="float: right;" class="next"> </a>
			               </span>
					   </div>
				     </div>  
				   </div>    		
    	       </div>
          </div>
    	 
    	        	<div class="wrap">
    	    	<div class="content-bottom-right">
    	    	<h3>Algunos productos</h3>
	            <div class="section group">
				  <div class="grid_1_of_4 images_1_of_4">
					 <h4><a href="Shell.aspx">Aceite Para Motos Shell 10w 40 Advance 4t </a></h4>
					  <a href="Shell.aspx"><img src="images/26.jpg" alt="" /></a>
					  <div class="price-details">
				       <div class="price-number">
							<p><span class="rupees">$12.000 </span></p>
					    </div>
					       		
							 <div class="clear"></div>
					</div>					 
				</div>
				<div class="grid_1_of_4 images_1_of_4">
					<h4><a href="Mobil.aspx">20w50 / 12 Super Moto 4t Mobil</a></h4>
					 <a href="Mobil.aspx"><img src="images/27.jpg" alt="" /></a>
					<div class="price-details">
				       <div class="price-number">
							<p><span class="rupees">$16.000 </span></p>
					    </div>
					       		
							 <div class="clear"></div>
					</div>
					 
				</div>
				<div class="grid_1_of_4 images_1_of_4">
					<h4><a href="Motul.aspx">Aceite Para Suspensión De Moto Motul Fork Oil 10w </a></h4>
					<a href="Motul.aspx"><img src="images/28.jpg" alt="" /></a>
					<div class="price-details">
				       <div class="price-number">
							<p><span class="rupees">$10.000 </span></p>
					    </div>
					       		
							 <div class="clear"></div>
					</div>
				    
				</div>
				<div class="grid_1_of_4 images_1_of_4">
					<h4><a href="Filtro aire.aspx">Auto filtro de aceite del coche 1R-0726 7N-7500 4P-2839 1R0726 </a></h4>
					<a href="Filtro aire.aspx"><img src="images/29.jpg" alt="" /></a>
					 <div class="price-details">
				       <div class="price-number">
							<p><span class="rupees">$50.000 </span></p>
					    </div>
					       		
							 <div class="clear"></div>
					</div>
				 </div>
			   </div>
			   <div class="section group">
				<div class="grid_1_of_4 images_1_of_4">
					 <h4><a href="Filtro.aspx">Venta De Filtro De Aceite Para Neon Mopar 94/05. </a></h4>
					  <a href="Filtro.aspx"><img src="images/30.jpg" alt="" /></a>
					  <div class="price-details">
				       <div class="price-number">
							<p><span class="rupees">$40.000 </span></p>
					    </div>
					       		
							 <div class="clear"></div>
					</div>					 
				</div>
				<div class="grid_1_of_4 images_1_of_4">
					<h4><a href="Agua.aspx">agaua para bateria </a></h4>
					 <a href="Agua.aspx"><img src="images/31.jpg" alt="" /></a>
					<div class="price-details">
				       <div class="price-number">
							<p><span class="rupees">$6.000 </span></p>
					    </div>
					       		
							 <div class="clear"></div>
					</div>
					 
				</div>
				<div class="grid_1_of_4 images_1_of_4">
					<h4><a href="Liquido.aspx">LÍQUIDO PARA FRENOS DOT-4 </a></h4>
					<a href="Liquido.aspx"><img src="images/32.jpg" alt="" /></a>
					<div class="price-details">
				       <div class="price-number">
							<p><span class="rupees">$8.000</span></p>
					    </div>
					       		
							 <div class="clear"></div>
					</div>
				    
				</div>
				<div class="grid_1_of_4 images_1_of_4">
					<h4><a href="Grasa.aspx">Grasa Azul </a></h4>
					<a href="Grasa.aspx"><img src="images/33.jpg" alt="" /></a>
					 <div class="price-details">
				       <div class="price-number">
							<p><span class="rupees">$12.000 </span></p>
					    </div>
					     </div>
					 </div>
					       		
							 <div class="clear"></div>
					 
                    
</asp:Content>

