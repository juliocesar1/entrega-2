﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="A.C.P.M.aspx.cs" Inherits="A_C_P_M" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="wrap">
     <div class="header_bottom">
			   
        <div class="slider-text">
   	<h2>A.C.P.M o DIESEL</h2>
			   	<p>El Diesel está diseñado para utilizarse como combustible en motores tipo diesel de automotores de trabajo medio y pesado que operan bajo condiciones de alta exigencia en vías y carreteras del país, o para generar energía mecánica y eléctrica, y en quemadores de hornos, secadores y calderas.

Encuéntrelo solo en las Estaciones de Servicio Esso y Mobil certificadas para ayudar a mantener su motor más limpio mejorando el desempeño.</p>
        </div>
         </div>
	  	      <div class="slider-img">
	  	      	<img src="images/acpm.jpg" alt="" />
	  	      </div>
	  	     <div class="clear"></div>
	      </div>
   		</div>
</asp:Content>

