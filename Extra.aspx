﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Extra.aspx.cs" Inherits="Extra" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <div class="wrap">
     <div class="header_bottom">
			   
        <div class="slider-text">
			   	<h2>Gasolina Extra</h2>
			   	<p>La Gasolina Extra oxigenada tiene un índice antidetonante de 89 (mínimo). Esta gasolina ha sido diseñada para utilizarse en motores de combustión interna de encendido por chispa y de alta relación de compresión (mayor de 9:1) y para la mayoría de los motores en cualquier altitud. </p>
	  	      </div>
         </div>
	  	      <div class="slider-img">
	  	      	<img src="images/extra.jpg" alt="" />
	  	      </div>
	  	     <div class="clear"></div>
	      </div>
   		</div>
   </div>
</asp:Content>

