﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="mision.aspx.cs" Inherits="mision" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="wrap">
     <div class="header_bottom">
			   <div class="slider-text">
			   	<h2>Mision</h2>
			   	<p>Generar servicios integrados a través de las ventas de combustibles, aceites, y aditivos; de una manera eficiente y eficaz con diferenciación de la competencia para generar un valor agregado a nuestros clientes.</p>
			   	</div>
	  	      </div>
	  	      <div class="slider-img">
	  	      	<img src="images/1.png" alt="" />
	  	      </div>
	  	     <div class="clear"></div>
	      </div>
   		</div>
   </div>
</asp:Content>

