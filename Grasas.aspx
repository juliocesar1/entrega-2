﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Grasas.aspx.cs" Inherits="Grasas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="wrap">
    <div class="header_bottom">
			   
        <div class="slider-text">
			   	<h2>Grasas</h2>
			   	<p>Las grasas  pueden brindar una estabilidad de primera categoría contra la oxidación y humedad, así como protección contra corrosión y características de fluidez, incluso a temperaturas muy bajas. </p>
	  	      </div>
        </div>
	  	      <div class="slider-img">
	  	      	<img src="images/25.jpg" alt="" />
	  	      </div>
	  	     <div class="clear"></div>
	      </div>
   		</div>
   </div>
</asp:Content>

