﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Vision.aspx.cs" Inherits="Vision" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="wrap">
     <div class="header_bottom">
			   <div class="slider-text">
			   	<h2>Vision</h2>
			   	<p>Ser la cadena líder en el mercado de los combustibles, aceites y aditivos contando con la preferencia de nuestros clientes aplicando las técnicas y tecnologías más innovadoras a nuestro alcance.</p>
			   	</div>
	  	      </div>
	  	      <div class="slider-img">
	  	      	<img src="images/1.png" alt="" />
	  	      </div>
	  	     <div class="clear"></div>
	      </div>
   		</div>
   </div>
</asp:Content>

