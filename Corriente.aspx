﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Corriente.aspx.cs" Inherits="Corriente" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="wrap">
    <div class="header_bottom">
			   <div class="slider-text">
			   	<h2>Gasolina Corriente</h2>
			   	<p>La Gasolina Corriente oxigenada tiene un índice antidetonante de 84 (mínimo). Esta gasolina está diseñada para utilizarse en motores de combustión interna de encendido por chispa y de baja relación de compresión (menos de 9:1). </p>
	  	      </div>
        </div>
	  	      <div class="slider-img">
	  	      	<img src="images/21.jpg" alt="" />
	  	      </div>
	  	     <div class="clear"></div>
	      </div>
   		</div>
   </div>
</asp:Content>

