﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Aceites.aspx.cs" Inherits="Aceites" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
       <div class="wrap">
 <div class="header_bottom">
			   <div class="slider-text">
			   	<h2>Aceite</h2>
			   	<p>La función del aceite es lubricar y proteger el motor, generando una película separadora de las partes móviles y disminuyendo así el desgaste. </p>
	  	      </div>
     </div>
	  	      <div class="slider-img">
	  	      	<img src="images/25.jpg" alt="" />
	  	      </div>
	  	     <div class="clear"></div>
	      </div>
   		</div>
   </div>
</asp:Content>

